FROM python:3.8-slim

#Setup working directory
WORKDIR /app

#Copy the current directory to container
COPY . /app/

RUN pip install flask flask-cors

EXPOSE 8000

CMD ["python","main.py"]