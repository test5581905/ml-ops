# ml-ops



## Continuous Integration and Deployment Pipeline Steps

This pipeline automates the process of taking code from version control, building it into a Docker image, deploying it, testing it, and cleaning up any resources used during the process.

## Prerequisites

- Docker installed on the Jenkins and EC2 host.
- SSH agent with access to the EC2 instance via SSH keys.
- Docker registry credentials configured in Jenkins.
- Pipeline Stages
- Clone Repository
- Jenkins checks out the main branch from the specified GitLab repository URL.



## Build Image

Jenkins builds a Docker image from the Dockerfile in the repository.
The built image is tagged with the name specified in imagename.

## Push Image

Jenkins tags the built Docker image with the current build number and "latest".
Jenkins pushes these tags to the Docker registry.

## Deploy and Test

Jenkins pulls the "latest" tagged image from the Docker registry to the EC2 instance.
Jenkins starts a new Docker container with the pulled image on the EC2 instance. It maps the container's internal port 80 to port 8090 on the EC2 host.
Each container is uniquely named using the build number to avoid conflicts.
Jenkins executes the tests.py script inside the running Docker container. This script should contain the necessary test cases for the application.



# Cleanup

Jenkins stops the Docker container that was started during the deployment stage.
Jenkins removes the Docker container to free up resources.
